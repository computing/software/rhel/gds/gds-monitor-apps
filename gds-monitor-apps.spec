%define packaging_base_name 	gds-monitors
%define version 2.19.8
%define release 1.1
%define daswg   /usr
%define prefix  %{daswg}
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

Name: 		gds-monitor-apps
Summary: 	gds-monitor-apps 2.19.8
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gds-dmt-devel >= 2.19.7
BuildRequires:  gds-gui-devel >= 2.19.7
BuildRequires:  gds-frameio-devel >= 2.19.6
BuildRequires:  gds-base-devel >= 2.19.8
BuildRequires:  gzip bzip2 libXpm-devel
BuildRequires:  ldas-tools-framecpp >= 2.5.8
BuildRequires:  ldas-tools-framecpp-devel >= 2.5.8
BuildRequires:  ldas-tools-al-devel
BuildRequires:  libmetaio-devel
BuildRequires:  root
BuildRequires:  readline-devel fftw-devel
BuildRequires:  jsoncpp-devel
Obsoletes: gds-monitors-base <= 2.19.2
Requires: gds-dmt-base
Requires: gds-frameio-base >= 2.19.6
Requires: gds-services >= 2.19.8
Requires: ldas-tools-framecpp
Requires: jsoncpp-devel
Prefix:		      %prefix

%description
GDS DMT monitor programs

%package -n %{packaging_base_name}-headers
Summary: 	GDS monitor header files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis

%description -n %{packaging_base_name}-headers
GDS monitor header files.

%package -n %{packaging_base_name}-devel
Summary: 	GDS monitor development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name} = %{version}
Requires:       %{packaging_base_name}-headers = %{version}
Obsoletes:      gds-monitors-devel <= 2.18.7

%description -n %{packaging_base_name}-devel
GDS monitor  development files.

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  --prefix=%prefix --libdir=%{_libdir} \
	           --includedir=%{prefix}/include/gds \
	           --enable-online --enable-dtt
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files
%defattr(-,root,root)
%{_bindir}/ape_stream
%{_bindir}/aperture
%{_bindir}/BicoMon
%{_bindir}/BicoViewer
%{_bindir}/BitTest
%{_bindir}/blrms_monitor
%{_bindir}/burstMon
%{_bindir}/callineMon
%{_bindir}/coherence_monitor
%{_bindir}/Cumulus
%{_bindir}/DEnvCorr
%{_bindir}/dac_adc_ovfl
%{_bindir}/dmt_wmovie
%{_bindir}/dmt_wplot
%{_bindir}/dmt_wscan
%{_bindir}/dmt_wsearch
%{_bindir}/dmt_wstream
%{_bindir}/dq-module
%{_bindir}/DuoTone
%{_bindir}/dvTest
%{_bindir}/earthquake_alarm
%{_bindir}/EndTimes
%{_bindir}/eqMon
%{_bindir}/HistCompr
%{_bindir}/InspiralMon
%{_bindir}/IRIG-B
%{_bindir}/kleineWelleM
%{_bindir}/LIGOLwMon
%{_bindir}/LightMon
%{_bindir}/LineMonitor
%{_bindir}/LockLoss
%{_bindir}/MultiVolt
%{_bindir}/NoiseFloorMonitor
%{_bindir}/NormTest
%{_bindir}/OmegaMon
%{_bindir}/PCalMon
%{_bindir}/PhotonCal
%{_bindir}/PlaneMon
%{_bindir}/PSLmon
%{_bindir}/PulsarMon
%{_bindir}/RayleighMonitor
%{_bindir}/SegGener
%{_bindir}/SenseMonitor
%{_bindir}/sense_monitor2
%{_bindir}/ShapeMon
%{_bindir}/SixtyHertzMon
%{_bindir}/SpectrumFold
%{_bindir}/Station
%{_bindir}/StochMon
%{_bindir}/StrainbandsMon
%{_bindir}/suspensionMon
%{_bindir}/TimeMon
%{_bindir}/trendview
%{_bindir}/TrigDsply
%{_bindir}/TrigSpec
%{_bindir}/WaveMon
%{_libdir}/libdqplugins.so*
%{_libdir}/libwpipe.so*
%{_datadir}/gds/Omega_c
%{_datadir}/gds/html-styles

%files -n %{packaging_base_name}-headers
%defattr(-,root,root)
%{_includedir}/gds/DQ_Bit.hh

%files -n %{packaging_base_name}-devel
%defattr(-,root,root)
%{_libdir}/libdqplugins.a
%{_libdir}/libwpipe.a

%changelog
* Tue Apr 19 2022 Edward Maros <ed.maros@ligo.org> - 2.19.8-1
- Updated for release as described in NEWS.md

* Fri Jan 28 2022 Edward Maros <ed.maros@ligo.org> - 2.19.7-1
- Updated for release as described in NEWS.md

* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.6-1
- Updated for release as described in NEWS.md

* Tue Feb 23 2021 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- Corrected Debian packaging

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Removed python configuration
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Split lowlatency as a separate package

* Thu Jun 11 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Removed FrameL
